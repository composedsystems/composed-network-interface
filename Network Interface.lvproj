﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="CCSymbols" Type="Str">VERBOSE_LOGGING,True;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">VERBOSE_LOGGING,False;</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Client Actor.lvlib" Type="Library" URL="../Examples/Client Actor/Client Actor.lvlib"/>
			<Item Name="EventConnectionCallback.lvclass" Type="LVClass" URL="../Examples/EventConnectionCallback/EventConnectionCallback.lvclass"/>
			<Item Name="EventMessageRxCallback.lvclass" Type="LVClass" URL="../Examples/EventMessageRxCallback/EventMessageRxCallback.lvclass"/>
			<Item Name="Example Multiplex Server.vi" Type="VI" URL="../Examples/Example Multiplex Server.vi"/>
			<Item Name="MultiplexSandbox.vi" Type="VI" URL="../Examples/MultiplexSandbox.vi"/>
			<Item Name="Sandbox.vi" Type="VI" URL="../Examples/Sandbox.vi"/>
		</Item>
		<Item Name="Framework" Type="Folder">
			<Item Name="Proxy Classes" Type="Folder">
				<Item Name="ProtectedConnectionReader.lvclass" Type="LVClass" URL="../Source/ProtectedConnectionReader/ProtectedConnectionReader.lvclass"/>
				<Item Name="ProtectedConnectionWriter.lvclass" Type="LVClass" URL="../Source/ProtectedConnectionWriter/ProtectedConnectionWriter.lvclass"/>
			</Item>
			<Item Name="TCP Interface" Type="Folder">
				<Item Name="TCP Interface.lvlib" Type="Library" URL="../Source/TCP Interface/TCP Interface.lvlib"/>
			</Item>
			<Item Name="Connection Reference.lvlib" Type="Library" URL="../Source/Connection Reference/Connection Reference.lvlib"/>
			<Item Name="ConnectionFactorySession.lvlib" Type="Library" URL="../Source/Connection Factory Session/ConnectionFactorySession.lvlib"/>
			<Item Name="ConnectionManager.lvlib" Type="Library" URL="../Source/ConnectionManager/ConnectionManager.lvlib"/>
			<Item Name="ConnectionReader.lvlib" Type="Library" URL="../Source/ConnectionReader/ConnectionReader.lvlib"/>
			<Item Name="Create Ref Message.lvclass" Type="LVClass" URL="../Source/Create Ref Message/Create Ref Message.lvclass"/>
			<Item Name="IConnectionFactory.lvclass" Type="LVClass" URL="../Source/IConnectionFactory/IConnectionFactory.lvclass"/>
			<Item Name="LoggedActor.lvclass" Type="LVClass" URL="../Source/Logged Actor/LoggedActor.lvclass"/>
			<Item Name="MessagingInterfaces.lvlib" Type="Library" URL="../Source/MessagingInterfaces/MessagingInterfaces.lvlib"/>
			<Item Name="MultiplexedConnectionHandler.lvlib" Type="Library" URL="../Source/MultiplexedConnectionHandler/MultiplexedConnectionHandler.lvlib"/>
			<Item Name="PersitantConnectionHandler.lvlib" Type="Library" URL="../Source/PersistantConnectionHandler/PersitantConnectionHandler.lvlib"/>
		</Item>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="event-logger" Type="Folder">
					<Item Name="Examples" Type="Folder">
						<Item Name="Logger Examples.lvlib" Type="Library" URL="../externals/cs-event-logger/Examples/Logger Examples.lvlib"/>
					</Item>
					<Item Name="Source" Type="Folder">
						<Item Name="Buffered Log Sink" Type="Folder">
							<Item Name="Buffered Log Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Buffered Log Sink/Buffered Log Sink.lvclass"/>
						</Item>
						<Item Name="Composed Log" Type="Folder">
							<Item Name="Singleton Event Log" Type="Folder">
								<Item Name="Singleton Event Log.lvlib" Type="Library" URL="../externals/cs-event-logger/Source/Composed Log/Singleton Event Log/Singleton Event Log.lvlib"/>
							</Item>
							<Item Name="Composed Log.lvlib" Type="Library" URL="../externals/cs-event-logger/Source/Composed Log/Composed Log.lvlib"/>
						</Item>
						<Item Name="ConsoleView" Type="Folder">
							<Item Name="ConsoleView.lvlib" Type="Library" URL="../externals/cs-event-logger/Source/ConsoleView/ConsoleView.lvlib"/>
						</Item>
						<Item Name="Filters" Type="Folder">
							<Item Name="Compound Filter" Type="Folder">
								<Item Name="Compound Filter.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Filters/Compound Filter/Compound Filter.lvclass"/>
							</Item>
							<Item Name="Event Keyword Filter" Type="Folder">
								<Item Name="Event Keyword Filter.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Filters/Event Keyword Filter/Event Keyword Filter.lvclass"/>
							</Item>
							<Item Name="Event Level Filter" Type="Folder">
								<Item Name="Event Level Filter.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Filters/Event Level Filter/Event Level Filter.lvclass"/>
							</Item>
							<Item Name="Event Source Filter" Type="Folder">
								<Item Name="Event Source Filter.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Filters/Event Source Filter/Event Source Filter.lvclass"/>
							</Item>
						</Item>
						<Item Name="IStringFormat" Type="Folder">
							<Item Name="IStringFormat.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/IStringFormat/IStringFormat.lvclass"/>
						</Item>
						<Item Name="LVQueue Sink" Type="Folder">
							<Item Name="LVQueue Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/LVQueue Sink/LVQueue Sink.lvclass"/>
						</Item>
						<Item Name="String Control Sink" Type="Folder">
							<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
						</Item>
						<Item Name="String Formats" Type="Folder">
							<Item Name="String Expression Format" Type="Folder">
								<Item Name="String Expression Format.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/String Formats/String Expression Format/String Expression Format.lvclass"/>
							</Item>
						</Item>
						<Item Name="String Log Sink" Type="Folder">
							<Item Name="String Log Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/String Log Sink/String Log Sink.lvclass"/>
						</Item>
						<Item Name="SystemLog Sink" Type="Folder">
							<Item Name="SystemLog Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/SystemLog Sink/SystemLog Sink.lvclass"/>
						</Item>
						<Item Name="Text File Sink" Type="Folder">
							<Item Name="Text File Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Text File Sink/Text File Sink.lvclass"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/event-logger/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/event-logger/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/event-logger/LICENSE"/>
					<Item Name="Readme.md" Type="Document" URL="../gpm_packages/@cs/event-logger/Readme.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Tests" Type="Folder">
			<Item Name="TestDoubles" Type="Folder">
				<Item Name="ConnectionFactoryDecorator" Type="Folder">
					<Item Name="ConnectionFactoryDecorator.lvclass" Type="LVClass" URL="../Tests/TestDoubles/ConnectionFactoryDecorator/ConnectionFactoryDecorator.lvclass"/>
				</Item>
				<Item Name="ConnectionFactoryReturnMultiple" Type="Folder">
					<Item Name="ConnectionFactoryReturnMultiple.lvclass" Type="LVClass" URL="../Tests/TestDoubles/ConnectionFactoryReturnMultiple/ConnectionFactoryReturnMultiple.lvclass"/>
				</Item>
				<Item Name="DecoratedConnection" Type="Folder">
					<Item Name="DecoratedConnection.lvclass" Type="LVClass" URL="../Tests/TestDoubles/DecoratedConnection/DecoratedConnection.lvclass"/>
				</Item>
				<Item Name="MockConnection_Counter" Type="Folder">
					<Item Name="MockConnection_Counter.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockConnection_Counter/MockConnection_Counter.lvclass"/>
				</Item>
				<Item Name="MockConnectionFactory" Type="Folder">
					<Item Name="MockConnectionFactory.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockConnectionFactory/MockConnectionFactory.lvclass"/>
				</Item>
				<Item Name="MockConnectionReader" Type="Folder">
					<Item Name="MockConnectionReader.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockConnectionReader/MockConnectionReader.lvclass"/>
				</Item>
				<Item Name="MockConnectionWriter" Type="Folder">
					<Item Name="MockConnectionWriter.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockConnectionWriter/MockConnectionWriter.lvclass"/>
				</Item>
				<Item Name="MockMessageProtocolNoRead" Type="Folder">
					<Item Name="MockMessageProtocolNoRead.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockMessageProtocolNoRead/MockMessageProtocolNoRead.lvclass"/>
				</Item>
				<Item Name="PublishInitializedEventConnection" Type="Folder">
					<Item Name="PublishInitializedEventConnection.lvclass" Type="LVClass" URL="../Tests/TestDoubles/PublishInitializedEventConnection/PublishInitializedEventConnection.lvclass"/>
				</Item>
				<Item Name="ReadByteUntilTimeoutProtocol" Type="Folder">
					<Item Name="ReadByteUntilTimeoutProtocol.lvclass" Type="LVClass" URL="../Tests/TestDoubles/ReadByteUntilTimeoutProtocol/ReadByteUntilTimeoutProtocol.lvclass"/>
				</Item>
				<Item Name="MockEnqueueNewConnectionPolicy.lvclass" Type="LVClass" URL="../Tests/TestDoubles/MockEnqueueNewConnectionPolicy/MockEnqueueNewConnectionPolicy.lvclass"/>
				<Item Name="One Shot Message Return.lvclass" Type="LVClass" URL="../Tests/TestDoubles/One Shot Message Return/One Shot Message Return.lvclass"/>
				<Item Name="QueuePacketReturn.lvclass" Type="LVClass" URL="../Tests/TestDoubles/QueuePacketReturn/QueuePacketReturn.lvclass"/>
			</Item>
			<Item Name="TestConnectionFactorySession.lvclass" Type="LVClass" URL="../Tests/TestConnectionFactorySession/TestConnectionFactorySession.lvclass"/>
			<Item Name="TestLoggedActor.lvclass" Type="LVClass" URL="../Tests/TestLoggedActor/TestLoggedActor.lvclass"/>
			<Item Name="TestNetworkMessenger.lvclass" Type="LVClass" URL="../Tests/TestNetworkMessenger/TestNetworkMessenger.lvclass"/>
		</Item>
		<Item Name="Network Messenger.lvlib" Type="Library" URL="../Source/Network Messenger/Network Messenger.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="skip.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/VI Tester/TestCase.llb/skip.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TestCase.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/VI Tester/TestCase.llb/TestCase.lvclass"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="WaitOnTestComplete.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/VI Tester/TestCase.llb/WaitOnTestComplete.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Add Aggregate Log.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Add Aggregate Log.vi"/>
			<Item Name="Add Sink.vim" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Add Sink.vim"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Buffered String Log Sink.lvclass" Type="LVClass" URL="../externals/cs-event-logger/Source/Buffered String Log Sink/Buffered String Log Sink.lvclass"/>
			<Item Name="Close Local Event Log.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Close Local Event Log.vi"/>
			<Item Name="Construct String Control Sink.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/String Control Sink/Construct String Control Sink.vi"/>
			<Item Name="IErrorFormat.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/IErrorFormat/IErrorFormat.lvclass"/>
			<Item Name="Initialize Local Log.vim" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Initialize Local Log.vim"/>
			<Item Name="Initialize Logger Instance.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Initialize Logger Instance.vi"/>
			<Item Name="Launch Worker Loop.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Application Logger/Launch Worker Loop.vi"/>
			<Item Name="Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Log Sink/Log Sink.lvclass"/>
			<Item Name="LoggerReference.ctl" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/LoggerReference.lvclass/LoggerReference.ctl"/>
			<Item Name="LoggerReference.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/LoggerReference.lvclass"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Return Aggregate Queues.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Return Aggregate Queues.vi"/>
			<Item Name="Send Add Sink.vi" Type="VI" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/LoggerReference/Send Add Sink.vi"/>
			<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
